# == Schema Information
#
# Table name: test_data
#
#  id         :integer          not null, primary key
#  date       :date
#  benchMark  :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class TestData < ActiveRecord::Base
	belongs_to :student
	has_many :tests
end
