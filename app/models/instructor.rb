# == Schema Information
#
# Table name: instructors
#
#  id         :integer          not null, primary key
#  first_name :string(255)
#  mid_name   :string(255)
#  last_name  :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class Instructor < ActiveRecord::Base
	belongs_to :stud_class
	has_one :term_data
end
