# == Schema Information
#
# Table name: term_data
#
#  id         :integer          not null, primary key
#  created_at :datetime
#  updated_at :datetime
#

class TermData < ActiveRecord::Base
	belongs_to :student
	belongs_to :stud_class
	belongs_to :instructor
	has_many :terms
end
