# == Schema Information
#
# Table name: students
#
#  id         :integer          not null, primary key
#  first_name :string(255)
#  mid_name   :string(255)
#  last_name  :string(255)
#  tier       :decimal(2, 2)
#  DOB        :date
#  created_at :datetime
#  updated_at :datetime
#

class Student < ActiveRecord::Base
	belongs_to :stud_class
	 has_one :test_data
	 has_one :term_data
end
