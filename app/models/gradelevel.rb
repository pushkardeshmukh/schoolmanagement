# == Schema Information
#
# Table name: gradelevels
#
#  id         :integer          not null, primary key
#  gradelevel :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class Gradelevel < ActiveRecord::Base
	belongs_to :stud_class
end
