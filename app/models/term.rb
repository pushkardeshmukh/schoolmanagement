# == Schema Information
#
# Table name: terms
#
#  id         :integer          not null, primary key
#  startDate  :date
#  endDate    :date
#  created_at :datetime
#  updated_at :datetime
#

class Term < ActiveRecord::Base
	belongs_to :term_data
	belongs_to :test_data
end
