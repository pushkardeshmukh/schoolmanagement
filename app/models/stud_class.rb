# == Schema Information
#
# Table name: stud_classes
#
#  id         :integer          not null, primary key
#  created_at :datetime
#  updated_at :datetime
#

class StudClass < ActiveRecord::Base
	has_many :students
	has_one :instructor
	has_many :gradelevels
	has_one :term_data
end
