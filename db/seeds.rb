# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
User.create(email:'test@admin.com',password:'12345678', role: 0)
User.create(email:'faculty@1.com',password:'faculty1', role: 1)
User.create(email:'faculty@2.com',password:'faculty2', role: 1)
User.create(email:'faculty@3.com',password:'faculty3', role: 1)
