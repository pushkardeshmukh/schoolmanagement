class CreateGradelevels < ActiveRecord::Migration
  def change
    create_table :gradelevels do |t|
      t.string :gradelevel

      t.timestamps
    end
  end
end
