class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.string :first_name
      t.string :mid_name
      t.string :last_name
      t.decimal :tier, :precision => 2, :scale => 2
      t.date :DOB
      t.timestamps
    end
  end
  
end
