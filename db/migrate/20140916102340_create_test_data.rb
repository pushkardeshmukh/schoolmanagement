class CreateTestData < ActiveRecord::Migration
  def change
    create_table :test_data do |t|
      t.date :date
      t.string :benchMark

      t.timestamps
    end
  end
end
